#include <iostream>
#define NULL 0
static int variabila = 99;
using namespace std;

//ATENTIE!!!
//Sunt punctate doar solutiile originale si individuale
//Orice incercare de frauda va duce la pierderea intregului punctaj de la seminar
//Signatura functiilor nu se poate modifica, doar continului lor

//WARNING!!!
//Only the original and individual solution will be graded
//Any attempt of copying the solution will lead to loosing the entire lab grade
//The header of the functions cannot be modified, just their body

//1. Pentru a testa proiectul de teste unitare, scrieti corpul aceste functii
//astfel incat sa returneze corect suma dintre parametrii x si y

//1. To test the unit test project add the necessary code
//to return the sum of the x and y parameters
long suma(int x, int y)
{
	return x + y;
}

//2. Definiti un pointer la float si initializati-l cu NULL
//returnati acest pointer

//2. Define a pointer to float and initialise it with NULL
//return this pointer
float* returnare_pointer_null() // returneaz adresaa cand ai functie cu poitner // cand ai fara, putem sa returnam valoarea cu * la rezultat.
{
	float* a = nullptr; //am pus steluta ca sa ne afiseze valoarea , fara steluta doar adresa!
	return a;
}

//3. Definiti un pointer la int si initializati-l cu adresa
//variabilei globale variabila, returnati acest pointer

//3. Define a pointer to int and initialiase it with
//the address of the global variable, return this pointer
int* returnare_pointer_adresa() 
{
	int *a = &variabila;
	return a;

	
}

//4. Functia de mai jos primeste ca si parametru un pointer la double
//returnati valoarea aflata la acea adresa

//4. The function bellow receives a pointer to double as parameter
//return the value located at that address
double returnare_valoare_de_la_adresa(double* pointer) 
{
	double q;
	q = *pointer;
	return q;
	return 0;
}

//5. Functia de mai jos primeste ca si parametru un pointer la int
//returnati adresa urmatorului intreg utilizand operatori specifici pointerilor

//5. The function bellow receives a pointer to int as parameter
//return the address of the next in by using the pointer specific operators
int* returnare_adresa_intreg_urmator(int* pointer) 
{
	return pointer + 1;
	return NULL;
	
}

//6. Functia de mai jos primeste ca si parametru un pointer la long
//returnati valoarea de la adresa mai mica cu 2 * sizeof(long) fata de aceasta
//utilizand operatori specific pointerilor

//6. The function bellow receives a pointer to long as parameter
//return the value located at an address lower with 2 * sizeof(long) related to this
//by using pointer specific operators
long returnare_adresa_long_precedent(long* pointer)
{
	return *(pointer - 2);
	return 0;
}

//7. Functia de mai jos primeste ca si parametrii 2 pointeri la int
//returnati numarul de intregi care incap intre cele doua adrese de memorie
//8. Asigurati-va ca rezultatul este tot timpul unul pozitiv

//7. The function bellow receives as parameters two pointers to int
//return how many integers can fit between the two addresses
//8. Please make sure that the result is allways above zero
int returnare_diferenta_pointeri(int* pointer1, int* pointer2) 
{
	if ((pointer1 - pointer2) > 0) {
		return pointer1 - pointer2;
	}
	else return -(pointer1 - pointer2);
	
}

//9. Functia de mai jos primeste ca si parametru un pointer la pointer la int
//acesta reprezinta adresa unei adrese la care se gaseste un intreg
//returnati acel intreg

//9. The function bellow receives as parameter a pointer to pointer to int
//representing the address of an address where an integer is located
//return the value of that integer
int returnare_valoare_adresa_de_adresa(int** pointer)
{
	int*** q = &pointer;
	return ***q;
}

//10. Functia de mai jos este similara functiei de mai sus cu o mica diferenta
//al doilea parametru ne spune tipul de pointer => I pentru intreg sau L pentru long
//transformati pointerul primit ca parametru in tipul corespunzator
//si apoi returnati valoarea obtinuta la fel ca in functia de mai sus

//10. The function bellow is similar to the function above except one thing
//the second parameter tells the type of pointer => I for int and L for long
//trasnform the first parameter in the coresponding type of pointer
//and then return the value obtained similar to the one from the previous function
long returneaza_valoarea_adresa_de_adresa_void(void** pointer, char tip_pointer) 
{
	if (tip_pointer == 'I') {
		int** intPtr = (int **)pointer;
		int*** q = &intPtr;
		return ***q;
	}
	else if (tip_pointer == 'L') {
		long** lngPtr = (long**)pointer;
		long*** q = &lngPtr;
		return ***q;
	}
	
}

int main() 
{
	//Playgroud
	//Testati aici functiile dorite si folositi debugger-ul pentru eventualele probleme
	//Test here the functions that you need and use the debugger to identify the problems
	cout << returnare_pointer_adresa();
}
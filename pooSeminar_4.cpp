#include <iostream>
#include<string>
using namespace std;

enum CuloareOchi { albastri, verzi, caprui, negrii = 5 };

union IdUnic
{
    short idScurt;
    int idMediu;
    long idLung;
};

struct Persoana
{
    string nume;
    string prenume;
    int varsta;
};

class Student
{
public:
    string nume;
    string prenume;
    int varsta;
    Student()
    {
        nume = "necunoscut";
        prenume = "necunoscut";
        varsta = 18;
        esteLaBuget = false;
    }
    Student(string nume, string _prenume)
    {
        this->nume = nume;
        this->prenume = _prenume;
    }
    ~Student() // asta e destructor
    {
        cout << "S-a apelat destructorul" << endl;
    }
    void repartizareBuget()
    {
        esteLaBuget = true;
    }
private:
    bool esteLaBuget = false;    
};

int main()
{
    Persoana persoana1;
    persoana1.varsta = 21;
    persoana1.nume = "Dita";
    persoana1.prenume = "Alexandru";

    Persoana persoana2;
    persoana2.varsta = 31;
    persoana2.nume = "Ditanu";
    persoana2.prenume = "Alexandre$cU";

    cout << persoana1.nume << endl;

    CuloareOchi culoare = caprui;
    cout << culoare << endl;;

    culoare = CuloareOchi::negrii;
    cout << culoare << endl;

    IdUnic id;
    id.idLung = 10000000;
    cout << id.idScurt << endl;

    Student student1;
    student1.nume = "Ditanu";
    student1.prenume = "Alexandru";
    student1.repartizareBuget();

    Student student2;
    student2.nume = "Adrian";

    Student student3("popescu", "ion");
    cout << student3.nume << endl;

    Student* ps; //8xC (ocupa 4 bytes) pointer la student
    ps = &student3;
    cout << (*ps).nume << endl;
    cout << ps->prenume << endl;
    Student v[7];
    ps = new Student(); // ...
    delete ps;
}


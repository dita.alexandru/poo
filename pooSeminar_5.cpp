#include <iostream>
#include <string>
using namespace std;

class Bicicleta
{
private:
	string producator;
	float greutate;
	int nrViteze;
	int* distante = nullptr;
	int nrExcursii = 0;

public:
	/*Bicicleta()
	{
		producator = "N/A";
		greutate = 5;
		nrViteze = 1;
	}*/

	Bicicleta(string _producator="NECUNOSCUT", float _greutate=5)
	{
		producator = _producator;
		greutate = _greutate;
		nrViteze = 1;
	}

	Bicicleta(string producator, float greutate, int nrViteze) : producator(producator),
		greutate(greutate), nrViteze(nrViteze) // pentru constante
	{

	}

	Bicicleta(string producator, int* distante, int nrExcursii)
	{	
		if (distante != nullptr && nrExcursii > 0)
		{
			this->distante = new int[nrExcursii];
			for (int i = 0; i < nrExcursii; i++)
			{
				this->distante[i] = distante[i];
			}
			this->nrExcursii = nrExcursii;
		}
		this->producator = producator;
		
	}

	Bicicleta(const Bicicleta& b) { //trasnmitem paramaetru prin valoare, astefel primim o copie a parametrului care este distrusa la final // trebuie sa modificam this-ul
		producator = b.producator;
		greutate = b.greutate;
		nrViteze = b.nrViteze;
		nrExcursii = b.nrExcursii;
		if (b.distante != nullptr && b.nrExcursii > 0)
		{
			distante = new int[b.nrExcursii];
			for (int i = 0; i < b.nrExcursii; i++)
			{
				distante[i] = b.distante[i];
			}
		}
	}

	~Bicicleta()
	{
		if (distante != nullptr)
		{
			delete[] distante;
			distante = nullptr;
		}
	}

	string getProducator() // returneaza un string
	{
		return producator;
	}

	void setProducator(string producator)
	{
		if (producator.length() > 0) //.length() este o metoda
			this->producator = producator;
	}

	float getGreutate()
	{
		return greutate;
	}

	void setGreutate(float greutate)
	{
		if (greutate > 0)
		{
			this->greutate = greutate;
		}
	}

};

int main()
{
	Bicicleta b1;
	Bicicleta b2("Pegas", 12);
	Bicicleta v[3];
	cout << v[0].getProducator() << endl;
	b1.setGreutate(20);
	cout << b1.getGreutate() << endl;
	Bicicleta b3("B`twin", 14, 18);
	cout << b3.getProducator() <<endl;
	Bicicleta* pb; // pointer la obiect, nu este obiect
	pb = &b1;
	pb = new Bicicleta();
	delete pb;
	pb = nullptr;
	pb = new Bicicleta("Pegas", 18);
	cout << pb->getGreutate() << endl;
	delete pb; // sterge ce e la adresa lui pb
	pb = nullptr;

	int d[3] = { 2, 7, 13 };
	Bicicleta b4("Rockrider", d, 3);
	d[0] = 99;

	Bicicleta b5 = b4; // modalitate explicita de a apela cpy consutrctor
	Bicicleta b6(b4); // modaliate explicita de a apela cpy constructor
}
 

